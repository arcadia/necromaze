﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoueChoixMinion : MonoBehaviour {
	int currentWheelMinion=0;
	Transform internWheel;
	// Use this for initialization
	void Start () {
		internWheel = this.gameObject.transform.GetChild (0);
	}

	// Update is called once per frame
	void Update () {

	}

	public void ChangeSelection(){
		if (Sequencer.Instance.currentMinion !=1) {
			internWheel.Rotate (0, 0, 4000 * Time.deltaTime);

		} else {
			internWheel.rotation = new Quaternion ();
		}

	}
}
