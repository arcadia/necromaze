﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsating : MonoBehaviour {
	public float period = 2f;
	public float clock = 0f;
	protected float direction;
	private RectTransform myTransform;
	// Use this for initialization
	void Start () {
		direction = Time.deltaTime;
		myTransform = this.GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () {
		clock += direction;
		if(clock>period){
			clock = period;
			direction *= -1;
		}
		if (clock < 0) {
			clock = 0;
			direction *= -1;
		}
		myTransform.localScale.Set(direction,direction,myTransform.localScale.z);
	}
}
