using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/*
Il faut mettre un RigidBody en Kinematic sur l'interrupteur, muni d'un Collider2D
"Trigger". Glisser la porte concernée dans le champ "gate" du SwitchGate, et le
collider s'enlevera de lui même.
*/
public class SwitchGate : MonoBehaviour {

	public GameObject gate;
	public SpriteRenderer openSwitch;
	public AudioSource aSource;

	private Sprite closeSwitch;

	// Use this for initialization
	void Start () {
		aSource = GetComponent<AudioSource>();
		closeSwitch = gate.GetComponent<SpriteRenderer>().sprite;
	}


	// Update is called once per frame
	void Update () {

	}

	void OnTriggerStay2D(Collider2D coll){
		if (gate.GetComponent<Collider2D> ().enabled == true)
		{
		Vector3 switchPos = this.gameObject.transform.position;
		double switchPos_x = switchPos[0];
		double switchPos_y = switchPos[1];

		Vector3 collPos = coll.gameObject.transform.position;
		double collPos_x = collPos[0];
		double collPos_y = collPos[1];

		double distance_x = Math.Abs(switchPos_x - collPos_x);
		double distance_y = Math.Abs(switchPos_y - collPos_y);
		double rayon = Math.Sqrt(distance_x*distance_x+distance_y*distance_y);
		if (rayon < 1){
			gate.GetComponent<Collider2D> ().enabled = false;
			gate.GetComponent<SpriteRenderer>().sprite = openSwitch.sprite;
			aSource.Play();
		}

		}
		//Ici on peut gérer l'animation de la porte avec gate.GetComponent<SpriteRenderer>()

	}

	void OnTriggerExit2D(Collider2D coll){
		if (gate.GetComponent<Collider2D> ().enabled == false)
		{
		gate.GetComponent<Collider2D> ().enabled = true;
		gate.GetComponent<SpriteRenderer>().sprite = closeSwitch;
		aSource.Play();

	}
	}
}
