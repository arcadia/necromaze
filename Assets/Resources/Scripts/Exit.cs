﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour {
    public string scene;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
			if (scene != "exitGame") {
				if (scene != SceneManager.GetActiveScene().name)
					Initiate.Fade(scene, Color.black, 1f);
				else 
					Sequencer.Instance.GoToScene(Globals.pathToLevels+scene);
			}
			else {
				 #if UNITY_EDITOR
					 // Application.Quit() does not work in the editor so
					 // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
					 UnityEditor.EditorApplication.isPlaying = false;
				 #else
					 Application.Quit();
				 #endif
			}
            
		}
    }
}
