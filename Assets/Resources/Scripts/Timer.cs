﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
 public class Timer : MonoBehaviour
 {
	 public int  Minutes = 0;
	 public int  Seconds = 0;
	 private int initMinutes;
	 private int initSeconds;
 
	 public Text    m_text;
	 private float   m_leftTime;
	 
	 private void Reset() {
		m_leftTime = GetInitialTime();
		Minutes = initMinutes;
		Seconds = initSeconds;
		
		if (m_leftTime > 0f)
		{
			m_text.text = "Temps : " + Minutes + ":" + Seconds.ToString("00");
		}
	 }
	 

	 private void Awake()
	 {
		
		initMinutes = Minutes;
		initSeconds = Seconds;
		m_leftTime = GetInitialTime();
		Minutes = GetLeftMinutes();
		Seconds = GetLeftSeconds();
		
		if (m_leftTime > 0f)
		{
			m_text.text = "Temps : " + Minutes + ":" + Seconds.ToString("00");
		}
		
	 }
 
	 private void Update()
	 {

			if ((SceneManager.GetActiveScene().name != "accueil") && 
			(SceneManager.GetActiveScene().name != "cinematique") && 
			(SceneManager.GetActiveScene().name != "tutoriel") &&
			(SceneManager.GetActiveScene().name != "lastScene")&&
			(SceneManager.GetActiveScene().name != "demo_accueil")&&
			(SceneManager.GetActiveScene().name != "demo_cinematique")&&
			(SceneManager.GetActiveScene().name != "demo_tutoriel")
			)
			{

				if (m_leftTime > 0f)
				{
					//  Update countdown clock
					m_leftTime -= Time.deltaTime;
					Minutes = GetLeftMinutes();
					Seconds = GetLeftSeconds();

				 //  Show current clock
				 if (m_leftTime > 0f)
				 {
					 m_text.text = "Temps : " + Minutes + ":" + Seconds.ToString("00");
				 }
				 else
				 {
					 //Game Over
					 Sequencer.Instance.GoToScene("gameOver");
					 //  The countdown clock has finished
					 m_text.text = "Temps : 0:00";
				 }
				}
						
			}
			else if ((SceneManager.GetActiveScene().name == "accueil") ||
			(SceneManager.GetActiveScene().name != "demo_accueil")) {
				Reset();
			} 
		
	 }
	 
 
	 private float GetInitialTime()
	 {
		 return Minutes * 60f + Seconds;
	 }
 
	 private int GetLeftMinutes()
	 {
		 return Mathf.FloorToInt(m_leftTime / 60f);
	 }
 
	 private int GetLeftSeconds()
	 {
		 return Mathf.FloorToInt(m_leftTime % 60f);
	 }
 }
