﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MovingObject {

	protected Pusher currentPusher;
	 public AudioSource aSource;

	public Dictionary<Direction,bool> accessibleDirections;
	public bool isFragile;
	private bool canMove=true;
	public SpriteRenderer waterCrate;

	// Use this for initialization
	protected override void Start () {
		aSource = GetComponent<AudioSource>();
		base.UpdateCrate();
		accessibleDirections = new Dictionary<Direction, bool> ();
		accessibleDirections.Add (Direction.up ,true);
		accessibleDirections.Add (Direction.down ,true);
		accessibleDirections.Add (Direction.left, true);
		accessibleDirections.Add (Direction.right , true);
	}

	// Update is called once per frame
	protected override void Update () {
		base.UpdateCrate();
	}

	protected override void Control ()
	{
		if (currentPusher == null) {
			StopMovement ();

		} else {
			/*
			Direction curDir=Direction.down;
			switch ((int)currentPusher.getDirection ().x) {
			case -1:
				curDir = Direction.left;
				break;
			case 1:
				curDir = Direction.right;
				break;
			}
			switch((int)currentPusher.getDirection().y){
			case -1:
				curDir = Direction.down;
				break;
			case 1:
				curDir = Direction.up;
				break;
			}
			*/
			//if (accessibleDirections [curDir]) {
			if(canMove){
				this.mySpeed = currentPusher.getSpeed ();
				this.myDirection = currentPusher.getDirection ();
			}
		}

	}

	void OnCollisionStay2D(Collision2D coll){
		if (coll.gameObject.tag == "Pusher") {
			currentPusher = coll.gameObject.GetComponent <Pusher> ();
            if (currentPusher != null)
            {
                currentPusher.Attach();
            }

		} else {
			StopMovement ();
			if (currentPusher != null) {
				currentPusher.Release ();
				//canMove = false;
			}
			currentPusher = null;
			accessibleDirections [lastFacedDirection] = false;
		}

	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Explosion" && isFragile) {
			this.Break ();
		}
		// Crate falls in water
		if (coll.gameObject.tag == "Water") {
			this.gameObject.transform.position = coll.gameObject.transform.position;
			gameObject.GetComponent<Collider2D> ().enabled = false;
			coll.gameObject.GetComponent<Collider2D> ().enabled = false;
			aSource.Play();

			// Change asset of crate on water
			this.gameObject.GetComponent<SpriteRenderer>().sprite = waterCrate.sprite;
			this.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "BKG";
			if (currentPusher != null)
				currentPusher.Release ();
			canMove = false;
			StopMovement();
		}

	}

	void OnCollisionExit2D(Collision2D coll){

		if (coll.gameObject.tag == "Pusher") {
			StopMovement ();
			currentPusher = null;
		}
	}

	void Break(){
		aSource.Play();
		Invoke("killit", 0.2f);
	}

	void killit()
	{
Destroy (this.gameObject);

	}

}
