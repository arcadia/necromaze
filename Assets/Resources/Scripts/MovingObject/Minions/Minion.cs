﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minion : MovingObject {
	public float maxDuration = 10f;
	protected float curDuration=0f;

	protected bool isSummoned = false;
	public bool canMove=false;
	
	//Animation of the Pusher
	protected Animator myAnimator;
	protected bool isMoving = true;
	
	// Use this for initialization
	protected virtual void Start () {
		base.Start ();
		curDuration=maxDuration;
		
		//Init animator
		myAnimator=gameObject.GetComponent<Animator> ();
		myAnimator.SetBool("isMoving", isMoving);
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		base.Update ();
		curDuration -= Time.deltaTime;
		if (curDuration <= 0f ){
			curDuration = maxDuration;
			Kill ();
		}
		
		//Animate the pusher in function of the direction
		Vector2Int dir = new Vector2Int((int)LDFToFloat ().x,(int)LDFToFloat().y);
		if (dir.x != myAnimator.GetInteger ("dirX")) {
			myAnimator.SetInteger ("dirX", dir.x);
		} else {
			myAnimator.SetInteger ("dirX", 0);

		}if (dir.y != myAnimator.GetInteger ("dirY")) {
			myAnimator.SetInteger ("dirY", dir.y);

		}

	}

	protected override void Control(){
		if (canMove) {
			mySpeed = maxSpeed;
		} else {
			StopMovement ();
		}
	}

	protected virtual void Summon(){
		isSummoned = true;

	}

	protected virtual void Kill(){

        Destroy (this.gameObject);
	}
		
}
