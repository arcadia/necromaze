﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomber : Minion {
	public GameObject explosionPrefab;
	public AudioSource aSource;
	// Use this for initialization
	protected override void Start () {
		aSource = GetComponent<AudioSource>();
		base.Start ();
		this.gameObject.tag = "Bomber";
	}

	// Update is called once per frame
	protected override void Update () {
		base.Update ();
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag != "Player") {
			StopMovement ();
			Invoke("Kill",1f);
		}

	}

	protected override void Control ()
	{
		base.Control();
	}

	protected override void Kill(){
		ExplosionFactory.generateExplosion (gameObject.transform.position, explosionPrefab);
		aSource.Play();
		Invoke("killit", 0.3f);
	}

	void killit()
	{
		base.Kill();

	}
}
