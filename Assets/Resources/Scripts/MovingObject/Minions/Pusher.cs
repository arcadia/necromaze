﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Pusher : Minion {

	public bool isPushing=false;

    public Vector3 pos;

    public Vector3 lastPos;

		public AudioSource aSource;

	protected Vector2 dir = new Vector2Int ();

	protected bool start = false;

    // Use this for initialization
    protected override void Start () {
			aSource = GetComponent<AudioSource>();
		base.Start ();
		this.gameObject.tag = "Pusher";
        lastPos = myPos;
    }

	// Update is called once per frame
	protected override void Update () {
		base.Update ();
		//Correction de l'axe
		this.StayOnDir(pos,lastPos);
		lastPos = pos;
	}

	protected override void Control(){
        pos = this.myPos;
		dir = this.myDirection;

		//Correction de l'axe
		this.StayOnDir(pos,lastPos);


		if (start)
		{
			// Teste la zone de collision
			if ( Math.Pow(Math.Pow(pos.x-lastPos.x,2) + Math.Pow(pos.y - lastPos.y, 2),0.5) < (this.maxSpeed[0] * Time.deltaTime/ 30f))
			{
				Kill();
			}

		}
		else
		{
			start=true;
		}


		// Teste pour vérifier que le pusher n'est pas dévié
		//if ((pos.x-lastPos.x !=0 && dir.x==0) || (pos.y-lastPos.y !=0 && dir.y==0))
		//{
			//Kill();

		//}

		// Ne pas prendre en compte les tremblements dus à l'animation
		//if ((lastPos.x*dir.x <= pos.x*dir.x && dir.x != 0) || (lastPos.y*dir.y <= pos.y*dir.y && dir.y != 0))
		lastPos = pos;

        base.Control ();
	}

	void OnCollisionEnter2D(Collision2D coll){
		if ((coll.gameObject.tag != "Pushable" && coll.gameObject.tag != "Player") || (coll.gameObject.tag == "Pushable" && isPushing)){
			StopMovement ();
            //Kill minion if it touches a wall or it is alreay pushing a crate
			Kill();
		}
	}

	public void Release(){
        Kill();
		isPushing = false;
	}

	public void Attach(){
		if (!aSource.loop)
		{
			aSource.Play();
		}
		isPushing = true;
		aSource.loop = true;

	}

	private void StayOnDir(Vector3 pos, Vector3 lastPos) {
		if (pos.x-lastPos.x !=0 && dir.x==0)
		{
			//Kill();
			pos.x = lastPos.x;
		}
		else if (pos.y-lastPos.y !=0 && dir.y==0)
		{
			//Kill();
			pos.y = lastPos.y;
		}
	}

}
