﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMoveScript : AnimatedMovingObject {
	public Minion[] allMinions;
	public int currentMinion = 0;
	private System.DateTime beginSummoning;
	public Follow[] allFollower;
		protected int newminion = 0;

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		isSummoning = false;
		myAnimator.SetBool("isSummoning", isSummoning);

	}

	// Update is called once per frame
	protected override void Update () {
		base.Update ();
		SummonAnimation();
	}

	protected override void Control(){
		//############################## HORIZONTAL KEY MANAGEMENT

		myDirection.x = Input.GetAxis ("Horizontal");
		myDirection.y = Input.GetAxis ("Vertical");
		if (myDirection.x != 0f) {
			mySpeed.x = maxSpeed.x;
		/*	if (myDirectionX > 0)
				lastFacedDirection = Direction.right;
			else
				lastFacedDirection = Direction.left;
				*/
		} else
			mySpeed.x = 0f;
		if (myDirection.y != 0f) {
			mySpeed.y = maxSpeed.y;
		} else
			mySpeed.y = 0;
		if ((Input.GetKeyUp (KeyCode.W)) || (Input.GetKeyUp (KeyCode.Alpha1)) || (Input.GetKeyUp (KeyCode.Space))) {
			// Invocation

			isSummoning = true;
			myAnimator.SetBool("isSummoning", isSummoning);
			beginSummoning = System.DateTime.Now;
			Summon (allMinions[currentMinion]);

		}
		
		//Changer de minion
		if ((Input.GetKeyUp (KeyCode.X)) || (Input.GetKeyUp (KeyCode.Alpha2)) || (Input.GetKeyUp (KeyCode.E)))
			SwitchMinion();
			
		//Reset button
		if ((Input.GetKeyUp (KeyCode.C)) || (Input.GetKeyUp (KeyCode.Alpha3)) || (Input.GetKeyUp (KeyCode.R)) )
		{
			Scene scene = SceneManager.GetActiveScene();
			Sequencer.Instance.GoToScene(scene.name);
		}
		
		//Go to Menu
		if ((Input.GetKeyUp (KeyCode.N)) || (Input.GetKeyUp (KeyCode.Alpha6)) || (Input.GetKeyUp (KeyCode.Escape)) )
		{
			Sequencer.Instance.GoToScene("accueil");
		}

	}

	public void Summon(Minion minion){
		Vector2 newPos = this.transform.position;
		newPos.x += 1f*LDFToFloat().x*1f;
		newPos.y += 1f*LDFToFloat().y*1f;
		Minion newMinion = Instantiate (minion, newPos, Quaternion.identity);
		newMinion.setDirections (LDFToFloat().x,LDFToFloat().y);
		newMinion.canMove = true;
	}

	//Animation of summoning duration
	public void SummonAnimation() {
		if ((isSummoning == true) && ((System.DateTime.Now - beginSummoning).TotalSeconds > 0.5)) {
            isSummoning = false;
			myAnimator.SetBool("isSummoning", isSummoning);
		}
	}

	public void SwitchMinion(){
		MinionSwitched();


	}



	public void MinionSwitched(){
		try{
		if (currentMinion==0)
			newminion=allMinions.Length-1;
		else
			newminion=currentMinion-1;


			
			allFollower[(currentMinion+1)%allMinions.Length].switchFollowed("player 1");
			allFollower[currentMinion].switchFollowed("F"+(newminion));
			Sequencer.Instance.MinionSwitched();
			currentMinion=(currentMinion+1)%allMinions.Length;
			Sequencer.Instance.currentMinion = currentMinion;
		}
		catch 
		{
		}


	}

}
