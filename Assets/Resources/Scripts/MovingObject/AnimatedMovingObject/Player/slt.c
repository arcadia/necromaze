#include <stdio.h>
#include <sys/mman.h>
#include <native/task.h>
#define TASK_PRIO 99 //highest RT priority
#define TASK_MODE 0 //no flags
#define TASK_STKSZ 0 //use default one
void task_body() {
    while(1)
    {
      rt_printf("Simple");
      rt_task_sleep(1000000000);
    }

}

void task_body2()
{
  while(1)
  {
    rt_printf(" Basique\n");
    rt_task_sleep(1000000000);
  }

}
int main(int argc, char *argv[]) {
  int err;
  int err2;
  RT_TASK task_desc;
  RT_TASK task_desc2;

  mlockall(MCL_CURRENT|MCL_FUTURE);// entire process’s memory is locked, for current and future mappings
  mlockall(MCL_CURRENT|MCL_FUTURE);// entire process’s memory is locked, for current and future mappings

  rt_print_auto_init(1);//necessary for rt_printf
  err=rt_task_create(&task_desc,"hello",TASK_STKSZ,TASK_PRIO,TASK_MODE);
  err2=rt_task_create(&task_desc2,"hello2",TASK_STKSZ,TASK_PRIO,TASK_MODE);
  if(err!=0) {
    printf("error rt_task_create\n");
    return;
  }

    rt_task_start(&task_desc,&task_body,NULL);
    rt_task_start(&task_desc2,&task_body2,NULL);


    while (1) {
      rt_task_sleep(1000000000);
    }
    rt_task_delete(&task_desc);
}
