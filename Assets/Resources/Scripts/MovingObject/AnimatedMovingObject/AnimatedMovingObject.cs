﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimatedMovingObject : MovingObject {

	protected Animator myAnimator;
    protected bool isMoving;
	protected bool isSummoning;
	// Use this for initialization
	protected virtual void Start () {
		base.Start ();
		
		isMoving = false;
		myAnimator=gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		base.UpdatePlayer ();
		Vector2Int dir = new Vector2Int((int)LDFToFloat ().x,(int)LDFToFloat().y);
		if (dir.x != myAnimator.GetInteger ("dirX")) {
			myAnimator.SetInteger ("dirX", dir.x);
		} else {
			myAnimator.SetInteger ("dirX", 0);

		}if (dir.y != myAnimator.GetInteger ("dirY")) {
			myAnimator.SetInteger ("dirY", dir.y);
		}
       
        if (mySpeed.x > 0f || mySpeed.y > 0f) {
            isMoving = true;
		}
        else {
            isMoving = false;
		}
		
		myAnimator.SetBool("isMoving", isMoving);
	}
		
}
