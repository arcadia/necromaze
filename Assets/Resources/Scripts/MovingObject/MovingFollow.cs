﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public abstract class MovingFollow : MonoBehaviour
{

    public enum Direction { up, down, left, right };

    protected Direction lastFacedDirection = Direction.down;

    public Vector2 maxSpeed = new Vector2(3, 3);

    public String folow;

    protected Vector2 mySpeed = new Vector2(0, 0);

    protected Vector2 myDirection = new Vector2Int();

    public Vector3 myPos;

    protected Vector3 posPlayer;

    protected float dx = 0f;

    protected float dy = 0f;

    protected float distance = 0f;

    protected float angle = 0f;

    protected float k = 25f;

    protected float distancemin = 0.5f;

    protected float force = 0f;

    protected float SpeedX = 0f;

    protected float SpeedY = 0f;

    protected float f = 5f;

    public Sprite[] skins;

    // Use this for initialization
    protected virtual void Start()
    {
        myPos = gameObject.transform.position;


    }

    // Update is called once per frame
    protected virtual void Update()
    {
        Control();
        Move();
        //Gère la direction
    }

    protected virtual void Control()
    {


    }

    protected virtual void Move()
    {
        try
        {
            posPlayer = GameObject.Find(folow).GetComponent<MovingObject>().myPos;
        }
        catch
        {
            posPlayer = GameObject.Find(folow).GetComponent<MovingFollow>().myPos;
        }

        myPos = gameObject.transform.position;



        dx = myPos.x - posPlayer[0];
        dy = myPos.y - posPlayer[1];
        if (dx != 0)
        {
            angle = Math.Abs((float)Math.Atan(dy / dx));
        }
        else{
            if (dy > 0)
            {
                angle = 3.1415f / 2;
            }
            else
            {
                angle = -3.1415f / 2;
            }

        }

        distance = (float)Math.Pow(dx * dx + dy * dy, 0.5f);

        if (distance<distancemin || ((distance< 2 * distancemin) && (folow == "player 1")))
        {
            force = 0;
        }
        else
        {
            force = -distance * k;
        }
        if (dx != 0)
        {
            SpeedX += Math.Abs(dx) / dx * force * (float)Math.Cos(angle) * Time.deltaTime - SpeedX / f;
        }
        else
        {
            SpeedX = 0f;
        }
        if (dy!=0)
        {
            SpeedY += Math.Abs(dy) / dy * force * (float)Math.Sin(angle) * Time.deltaTime - SpeedY / f;
        }
        else
        {
            SpeedY = 0f;
        }



        myPos.x += SpeedX * Time.deltaTime;
        myPos.y += SpeedY * Time.deltaTime;
        gameObject.transform.position = myPos;

    }

    public void IsSummoned(String followed, int numFollower){
	  SpriteRenderer mySR = this.gameObject.GetComponent<SpriteRenderer>();
	  mySR.sprite = skins[numFollower%skins.Length];
	  folow=followed;
	}

    public void switchFollowed(String newF){

	  folow=newF;
	}



}
