﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFactory  {
	//intensity à utiliser pour de plus grosses explosions
	public static void generateExplosion(Vector2 pos, GameObject explosionPrefab){
		foreach (int i in new int[] {-1,0,1}) {
			Object.Instantiate (explosionPrefab, new Vector2(pos.x+i,pos.y), Quaternion.identity);
			if(i!=0)
				Object.Instantiate (explosionPrefab, new Vector2(pos.x,pos.y+i), Quaternion.identity);
		}
	}
}
