﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicHelper : MonoBehaviour {
	public static MusicHelper Instance;

	private bool muted=false;
	private bool hasSwitched = true;
	private float startVolume;

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this.gameObject);

		if (Instance != null)
		{
			Debug.LogError("Multiple instances of SoundEffectsHelper!");
			Destroy(this.gameObject);
		}
		else {
			startVolume=this.GetComponent<AudioSource> ().volume;
			Instance = this;
		}
		
	}

	// Update is called once per frame
	void Update () {
		if(hasSwitched){
			if (muted)
				this.GetComponent<AudioSource> ().volume = 0f;
			else
				this.GetComponent<AudioSource> ().volume = startVolume;
			hasSwitched = false;
		}
	}

	public void mute(){
		hasSwitched = true;
		muted = true;
	}

	public void unmute(){
		hasSwitched = true;
		muted = false;
	}
}
