﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollText : MonoBehaviour {

	public Vector2 scrollPosition = Vector2.zero;
    public int nbLine = 1;
    public Text myText;
    void OnGUI()
    {
        scrollPosition.y += 0.5f * Time.deltaTime;
        if (scrollPosition.y > nbLine * 50.0f) scrollPosition.y = 0.0f;

        GUI.contentColor = Color.white;
        GUI.BeginScrollView(new Rect(20, 200, 440, 490), scrollPosition, new Rect(0, 0, 410, nbLine * 50.0f), false, true);
        GUI.EndScrollView();
    }

}
