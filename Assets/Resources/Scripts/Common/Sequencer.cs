﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sequencer : MonoBehaviour {
	//SCRIPT OU METTRE TOUT CE QUI CORRESPOND AU GAMEPLAY (changement de scenes, variables et méthodes accessibles à tous)
	//Design pattern Singleton

	public static Sequencer Instance;

	public GameObject wheelChosenMinionGO;
	public GameObject playerGO;

	public PlayerMoveScript player;

	public int currentMinion=0; // Minion selectionné par le joueur

	int minionsNumber;

	protected int newminion;

	public Minion[] minion;


	// Use this for initialization
	void Awake () {
		
		if (Instance != null)
		{
			Debug.LogError("Multiple instances of Sequencer");
		}
		Instance = this;
		minionsNumber = playerGO.GetComponent<PlayerMoveScript> ().allMinions.Length;
	}

	// Update is called once per frame
	void Update () {

	}

	public void MinionSwitched(){
		wheelChosenMinionGO.GetComponent<RoueChoixMinion>().ChangeSelection ();

	}

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }


}
