﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
	Animator myAnimator;
	// Use this for initialization
	void Start () {
		myAnimator = this.gameObject.GetComponent<Animator> ();
		Invoke ("Kill",1f);
	}
	
	// Update is called once per frame
	void Update () {
		Invoke("Kill",1f);
	}

	void Kill(){
		Destroy (this.gameObject);
	}


}
