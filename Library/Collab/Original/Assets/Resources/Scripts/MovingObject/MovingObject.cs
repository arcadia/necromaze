﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class MovingObject : MonoBehaviour {

	public enum Direction {up,down,left,right};

	protected Direction lastFacedDirection = Direction.down;

	public Vector2 maxSpeed=new Vector2(3,3);

	protected Vector2 mySpeed = new Vector2 (0, 0);

	protected Vector2 myDirection = new Vector2Int ();

	public Vector3 myPos;
    





    // Use this for initialization
    protected virtual void Start () {
		myPos = gameObject.transform.position;

    }
		
	// Update is called once per frame
	protected virtual void Update () {
		Control ();
        Move();
		//Gère la direction
		ManageDirection();
	}

    protected virtual void UpdatePlayer(String type="Player")
    {
        Control();
        movePlayer(type);
        //Gère la direction
        ManageDirection();
    }

    protected abstract void Control ();

    protected virtual void Move()
    {
        myPos = gameObject.transform.position;
        myPos.x += myDirection.x * mySpeed.x * Time.deltaTime;
        myPos.y += myDirection.y * mySpeed.y * Time.deltaTime;
        gameObject.transform.position = myPos;
    }

	protected virtual void movePlayer(string type){
        BoxCollider2D m_Collider;
        Vector3 m_Size;
        Vector2 offset;
        LayerMask boardTiles = 1 << LayerMask.NameToLayer("Ignore Raycast");
        boardTiles = ~boardTiles;
        myPos =gameObject.transform.position;

        m_Collider = GetComponent<BoxCollider2D>();
        offset = GetComponent<Collider2D>().offset;
        m_Size = m_Collider.bounds.size;
        float valeur = myDirection.x / Math.Abs(myDirection.x);
        Vector2 dir = new Vector2(valeur, 0);
        Vector2 pos = new Vector2(myPos[0]+ valeur*(m_Size[0]/2+ offset [0]+ 0.11f), myPos[1]);
        RaycastHit2D hit = Physics2D.Raycast(pos, dir, 0.1f, boardTiles);
        if (!hit.collider)
        {
            myPos.x += myDirection.x * mySpeed.x * Time.deltaTime;
        }
        valeur = myDirection.y / Math.Abs(myDirection.y);
        dir = new Vector2(0, myDirection.y / Math.Abs(myDirection.y));
        pos = new Vector2(myPos[0], myPos[1] + valeur* (m_Size[1] / 2 + offset[1]+ 0.11f));
        hit = Physics2D.Raycast(pos, dir, 0.1f, boardTiles);
        if (!hit.collider)
        {
            myPos.y += myDirection.y * mySpeed.y * Time.deltaTime;
        }
        
       
        gameObject.transform.position = myPos;


        if ((type == "Caisse") && (hit.collider.gameObject.tag == "Water"))
        {
            //Move();
        }


    }

	protected virtual void StopMovement(){
		mySpeed=new Vector2(0f,0f);
		myDirection.x = 0f;
		myDirection.y = 0f;
	}




    protected virtual void ManageDirection(){
		if (myDirection.x != 0) {
			if (myDirection.x > 0f)
				lastFacedDirection = Direction.right;
			else
				lastFacedDirection = Direction.left;
		}
		if (myDirection.y != 0) {
			if (myDirection.y > 0f)
				lastFacedDirection = Direction.up;
			else
				lastFacedDirection = Direction.down;
		}
		if (mySpeed.y==0f)
			myDirection.y = 0f;
		if (mySpeed.x==0f)
			myDirection.x = 0f;
	}

	public Vector2 getSpeed(){
		return mySpeed;
	}

	public float getSpeedX(){
		return mySpeed.x;
	}

	public float getSpeedY(){
		return mySpeed.y;
	}

	public Vector2 getDirection(){
		return myDirection;
	}
	public float getDirectionX(){
		return myDirection.x;
	}

	public float getDirectionY(){
		return myDirection.y;
	}

	public void setDirection(Vector2 dir){
		myDirection = dir;
	}
	public void setDirectionX(float dir){
		myDirection.x = dir;
	}

	public void setDirectionY(float dir){
		myDirection.y = dir;
	}

	public void setDirections(float dirX, float dirY){
		myDirection.x = dirX;
		myDirection.y = dirY;
	}

	//Convertit la dernière direction (Last Faced Direction) en vector2
	public Vector2 LDFToFloat(){
		Vector2 dir = new Vector2 (0f, 0f);
		switch (lastFacedDirection) {
		case Direction.up:
			dir.y = 1f;
			break;
		case Direction.down:
			dir.y = -1f;
			break;
		case Direction.right:
			dir.x = 1f;
			break;
		case Direction.left:
			dir.x = -1f;
			break;
		}
		return dir;
	}
		 
}
